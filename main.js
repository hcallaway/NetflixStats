/* Go to https://www.netflix.com/WiViewingActivity and run this in the console */

(function() {
    let fetchAllViewedItems = function() {
        let deferred = jQuery.Deferred();
        let viewedItems = [];
        (function fetchPage(page) {
        data = netflix.reactContext.models.serverDefs.data;
        url = data.SHAKTI_API_ROOT + '/' + data.BUILD_IDENTIFIER;
        jQuery.getJSON(url + '/viewingactivity?pg=' + page).done(function(json) {
            viewedItems = viewedItems.concat(json.viewedItems);
            console.log('Fetched %s viewed items', viewedItems.length);
            if (json.viewedItems.length == json.size) {
            fetchPage(++page);
            } else {
            deferred.resolve(viewedItems);
            }
        }).fail(deferred.reject);
        })(0);
        return deferred.promise();
    };
fetchAllViewedItems().then(function(viewedItems) {
    let totalTime = viewedItems.reduce(function(runningTotal, viewedItem) {
    return runningTotal + viewedItem.bookmark;
    }, 0);
    let days = Math.floor(totalTime / 60 / 60 / 24);
    let hours = Math.floor((totalTime / 60 / 60) % 24);
    let minutes = Math.round((totalTime / 60) % 60);
    console.log('According to your viewing history, you have cumulatively watched %i days, %i hours and %i minutes of Netflix', days, hours, minutes);
});
})();